package com.gb.versusapi.configure;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

// 기능 묶어주기
@Component
// implements: 상세기준 구현해주는 역할
public class CustomAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    // request: 고객이 요청한 데이터 주기, response: 서버가 고객에게 데이터 주기
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {

        // 괄호 안의 주소로 데이터 전송
        // ExceptionController 안에 있는 주소
        response.sendRedirect("/exception/access-denied");
    }
}
