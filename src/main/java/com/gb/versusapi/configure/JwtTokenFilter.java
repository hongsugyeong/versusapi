package com.gb.versusapi.configure;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import java.io.IOException;

@Component
@RequiredArgsConstructor
// 필터의 종류가 여러 개이기 때문에 bean에 등록 먼저 하기 (*필터의 종류와는 상관없이 등록하기)
public class JwtTokenFilter extends GenericFilterBean {
    private final JwtTokenProvider jwtTokenProvider;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String tokenFull = jwtTokenProvider.resolveToken((HttpServletRequest) servletRequest);
        String token = "";

        // resolve 값이 없거나 Bearer로 시작하지 않는다면
        if (tokenFull == null || !tokenFull.startsWith("Bearer ")) {
            filterChain.doFilter(servletRequest, servletResponse);
            // return 타입이 void로 return을 만나는 순간 끝남
            return;
        }

        // 사용자가 실수할 경우 대비
        // split: 띄어쓰기
        // trim: 앞뒤 공백 제거
        token = tokenFull.split(" ")[1].trim();
        // 제공자에게 토큰 검사 부탁하기
        if (!jwtTokenProvider.validateToken(token)) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        // 토큰을 통해 일회용 출입증 발급 받기
        Authentication authentication = jwtTokenProvider.getAuthentication(token);
        // Holder: 고정, Context: 환경 => 해당 환경에 일회용 출입증 고정시키기
        // 서비스와 컨트롤러 등에서 고정된 것 확인 후 아래 코드 실행
        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
