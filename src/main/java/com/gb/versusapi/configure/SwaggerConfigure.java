package com.gb.versusapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;

@OpenAPIDefinition(
        info = @Info(title = "versus App",
                description = "a 대 b 누가 더 인기 있나",
                version = "vi"))
@RequiredArgsConstructor
@Configuration
public class SwaggerConfigure {

    @Bean
    public GroupedOpenApi v1OpenApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("versus API v1")
                .pathsToMatch(paths)
                .build();
    }
}
