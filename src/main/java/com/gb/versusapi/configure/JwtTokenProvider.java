package com.gb.versusapi.configure;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class JwtTokenProvider {
    private final UserDetailsService userDetailsService;

    @Value("${spring.jwt.secret}")
    private String secretKey;

    // 구조 만들기
    @PostConstruct
    protected void init() {

        // Base64: 예전에 쓰던 단방향 암호화 기법 *얘 자체가 인코딩 방식을 씀
        // secretKey 안에 있는 데이터를 갖고 문자열로 변환 뒤 Base64를 통해 인코더 시키기
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    // 토큰 생성하기
    public String createToken(String username, String role, String type) {
        // Claims: 주장하다
            // 보안 토큰을 주장을 통해 나라는 신원 밝히기
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("role", role);

        // 토큰 유효시간 설정하기
        Date now = new Date();
        long tokenValiMillisecond = 1000L*60*60*10;
        if (type.equals("APP")) tokenValiMillisecond = 1000L*60*60*24*365;
        return Jwts.builder()
                .setClaims(claims) // 내 신원 밝히기
                .setIssuedAt(now) // 언제 발급되었는지
                .setExpiration(new Date(now.getTime() + tokenValiMillisecond)) // 언제 만료되었는지
                .signWith(SignatureAlgorithm.HS256, secretKey) // HS256을 이용해 사인하기
                .compact(); // 토큰 생성하기 (마무리 단계)
    }

    // 토큰 분석하여 인증정보 가져오기
    // loadUserByUsername 호출을 통해 UserDetails 받음
    // principal: 중요한
    // Authentication: 일회용 출입증
    public Authentication getAuthentication(String token) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(getUsername(token));

        // 인증 정보 만들기
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    // 토큰 파싱하여 인증정보 가져오기
    // 토큰 생성시 username은 subject에 넣은 것 확인하기
    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }

    // resolve: 반환된 성공의 결과물
        // resolveToken: 성공한 토큰
    public String resolveToken(HttpServletRequest request) {
        // 헤더에서 토큰값만 가져오기 (*요청 성공해야지 가져올 수 있음)
        return request.getHeader(HttpHeaders.AUTHORIZATION);
    }


    public boolean validateToken(String jwtToken) {
        // 유지보수 측면에서 try catch 씀
            // 문제가 생기면 던지도록 설정함
        try {
            // 토큰이 정상적인지 검사하기
            Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(jwtToken);
            // 유효시간 검사하여 유효시간 지났으면 false 주기
            return !claims.getBody().getExpiration().before(new Date());
        } catch (Exception e) {
            return false;
        }
    }
}
