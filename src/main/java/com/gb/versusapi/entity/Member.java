package com.gb.versusapi.entity;

import com.gb.versusapi.enums.Gender;
import com.gb.versusapi.enums.Grade;
import com.gb.versusapi.enums.MemberRating;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

@Entity
@Getter
@Setter
// 권한 부여하기
public class Member implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private MemberRating memberRating;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private Grade grade;

    @Column(nullable = false, length = 15, unique = true)
    private String username;

    @Column(nullable = false, length = 15)
    private String password;

    @Column(nullable = false, length = 10, unique = true)
    private String nickName;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    private LocalDate dateBirth;

    @Column(nullable = false, length = 13, unique = true)
    private String phoneNumber;

    @Column(nullable = false, length = 5)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @Column(nullable = false, length = 30, unique = true)
    private String email;

    @Column(nullable = false)
    private LocalDateTime dateUser;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // 꼭 객체화한 뒤 메소드 호출하기,, => memberRating 됨, MemberRating 안됨
        // 회원 등급에 대한 권한 부여하기
        return Collections.singleton(new SimpleGrantedAuthority(memberRating.toString()));
    }

    @Override
    // 계정 만료 여부
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    // 계정 정지 여부
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    // 신원 인증 만료 여부
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    // 활성화 여부
    public boolean isEnabled() {
        return true;
    }
}
