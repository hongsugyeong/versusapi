package com.gb.versusapi.controller;

import com.gb.versusapi.entity.Board;
import com.gb.versusapi.entity.VoteBoard;
import com.gb.versusapi.model.SingleResult;
import com.gb.versusapi.model.comment.CommentResponse;
import com.gb.versusapi.service.BoardService;
import com.gb.versusapi.service.CommentService;
import com.gb.versusapi.service.VoteBoardService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/comment")
public class CommentController {
    private final CommentService commentService;
    private final BoardService boardService;
    private final VoteBoardService voteBoardService;

    // 댓글 작성한 게시글 조회
    @GetMapping("/detail/board-id/comment-id/vote-board-id/{boardId}/{commentId}/{voteBoardId}")
    public SingleResult<CommentResponse> getCommentResponse (@PathVariable Long boardId , @PathVariable long commentId, @PathVariable Long voteBoardId) {

        Board id = boardService.getBoardId(boardId);
        VoteBoard vId = voteBoardService.getVoteBoardId(voteBoardId);

        CommentResponse result = commentService.getCommentResponse(id, commentId ,vId);

        SingleResult<CommentResponse> response = new SingleResult<>();
        response.setCode(0);
        response.setMsg("성공하였습니다.");
        response.setData(result);

        return response;
    }

    // 댓글 규제용 삭제
    @DeleteMapping("/delete/comment-id/{commentId}")
    public String delComment (@PathVariable long commentId) {
        commentService.delComment(commentId);

        return "규제에 벗어난 댓글을 삭제하였습니다.";
    }
}
