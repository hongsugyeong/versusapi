package com.gb.versusapi.controller;

import com.gb.versusapi.entity.Member;
import com.gb.versusapi.entity.RecommendBoard;
import com.gb.versusapi.model.ListResult;
import com.gb.versusapi.model.recommendBoard.BoardIsLikeResponse;
import com.gb.versusapi.model.recommendBoard.RecommendBoardList;
import com.gb.versusapi.service.MemberService;
import com.gb.versusapi.service.RecommendBoardService;
import com.gb.versusapi.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/recommend-board")
public class RecommendBoardController {
    private final RecommendBoardService recommendBoardService;
    private final MemberService memberService;

    // 좋아요한 게시글 보기
    @GetMapping("/check/isLike/member-id/{memberId}")
    public List<RecommendBoardList> getIsLikeBoard (@PathVariable long memberId) {
        Member id = memberService.getMemberId(memberId);
        return recommendBoardService.getIsLikeBoard(id);
    }
}
