package com.gb.versusapi.controller;

import com.gb.versusapi.model.CommonResult;
import com.gb.versusapi.model.ListResult;
import com.gb.versusapi.model.forbid.ForbidCreateRequest;
import com.gb.versusapi.model.forbid.ForbidList;
import com.gb.versusapi.service.ForbidService;
import com.gb.versusapi.service.ResponseService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/forbid")
public class ForbidController {
    private final ForbidService forbidService;

    // 금지어 추가
    @PostMapping("/new")
    public CommonResult setForbid (@RequestBody ForbidCreateRequest request) {
        forbidService.setForbid(request);

        return ResponseService.getSuccessResult();
    }

    // 금지어 조회
    @GetMapping("/all")
    public ListResult<ForbidList> getForbids () {
        List<ForbidList> list = forbidService.getForbids();

        ListResult<ForbidList> response = new ListResult<>();
        response.setList(list);
        response.setTotalCount(list.size());
        response.setCode(0);
        response.setMsg("성공");

        return response;
    }

    // 금지어 삭제
    @DeleteMapping("/delete/forbid-id/{forbidId}")
    public String delForbid (@PathVariable long forbidId) {
        forbidService.delForbid(forbidId);

        return "금칙어를 삭제하였습니다.";
    }

}
