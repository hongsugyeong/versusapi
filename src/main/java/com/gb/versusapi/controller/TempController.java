package com.gb.versusapi.controller;

import com.gb.versusapi.model.CommonResult;
import com.gb.versusapi.service.ResponseService;
import com.gb.versusapi.service.TempService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/temp")
public class TempController {
    private final TempService tempService;


    @PostMapping("/file-upload")
    public CommonResult sentRentCarBuFile(@RequestParam("csvFile")MultipartFile csvFile) throws IOException {
        tempService.setRentCarByFile(csvFile);

        return ResponseService.getSingleResult(tempService);
    }
}
