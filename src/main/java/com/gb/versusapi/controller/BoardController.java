package com.gb.versusapi.controller;

import com.gb.versusapi.model.ListResult;
import com.gb.versusapi.model.board.BoardList;
import com.gb.versusapi.model.forbid.CategoryChangeRequest;
import com.gb.versusapi.service.BoardService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board")
public class BoardController {
    private final BoardService boardService;

    // 작성한 게시글 리스트 보기
    @GetMapping("/all")
    public ListResult<BoardList> getBoards () {
        List<BoardList> list = boardService.getBoards();

        ListResult<BoardList> response = new ListResult<>();
        response.setList(list);
        response.setTotalCount(list.size());
        response.setCode(0);
        response.setMsg("성공하였습니다.");

        return response;
    }

    // 게시글 카테고리 변경
    @PutMapping("/change/category/board-id/{boardId}")
    public String putCategory (@PathVariable long boardId, @RequestBody CategoryChangeRequest request) {
        boardService.putCategory(boardId, request);

        return "카테고리를 변경하였습니다.";
    }
}
