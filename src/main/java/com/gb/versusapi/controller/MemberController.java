package com.gb.versusapi.controller;

import com.gb.versusapi.model.member.MemberDupCheckResponse;
import com.gb.versusapi.model.member.MemberJoinRequest;
import com.gb.versusapi.model.member.MemberNickNameChangeRequest;
import com.gb.versusapi.model.member.MemberPasswordChangeRequest;
import com.gb.versusapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    // 회원가입
    @PostMapping("/join")
    public String setMember (@RequestBody MemberJoinRequest request) throws Exception {
        memberService.setMemberJoin(request);

        return "회원가입이 완료되었습니다.";
    }

    // 신규 회원 판단
    @GetMapping("/check/id")
    public MemberDupCheckResponse getMemberIdCheck (@RequestParam(name = "username") String username) {
        return memberService.getMemberIdDupCheck(username);
    }

    // 닉네임 변경
    @PutMapping("/change/nick-name/member-id/{memberId}")
    public String putNickName (@PathVariable long memberId, @RequestBody MemberNickNameChangeRequest request) {
        memberService.putNickName(memberId, request);

        return "닉네임이 변경되었습니다.";
    }

    // 비밀번호 변경
    @PutMapping("/change/password/member-id/{memberId}")
    public String putPassword (@PathVariable long memberId, @RequestBody MemberPasswordChangeRequest request) throws Exception {
        memberService.putPassword(memberId, request);

        return "비밀번호가 변경되었습니다.";
    }
}
