package com.gb.versusapi.service;

import com.gb.versusapi.enums.ResultCode;
import com.gb.versusapi.model.CommonResult;
import com.gb.versusapi.model.SingleResult;
import org.springframework.stereotype.Service;

@Service
public class ResponseService {

    public static CommonResult getSuccessResult () {
        CommonResult result = new CommonResult();
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());

        return result;
    }

    public static <T> SingleResult<T> getSingleResult(T data) {
        SingleResult<T> result = new SingleResult<>();
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.FAILED.getMsg());
        result.setData(data);

        return result;
    }
}
