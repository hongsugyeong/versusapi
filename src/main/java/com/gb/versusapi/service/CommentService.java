package com.gb.versusapi.service;

import com.gb.versusapi.entity.Board;
import com.gb.versusapi.entity.Comment;
import com.gb.versusapi.entity.VoteBoard;
import com.gb.versusapi.model.comment.CommentResponse;
import com.gb.versusapi.repository.CommentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CommentService {
    private final CommentRepository commentRepository;

    /**
     * 작성한 댓글 조회하여 댓글과 게시글 리스트, 투표한 거 보여주기
     *
     * @param board 작성한 댓글의 해당 게시글
     * @param id 작성한 댓글의 id
     * @param voteBoard 투표한 게시글
     * @return CommentResponse 모델
     */
    public CommentResponse getCommentResponse (Board board, long id, VoteBoard voteBoard) {
        Comment comment = commentRepository.findById(id).orElseThrow();

        CommentResponse response = new CommentResponse();

        response.setMemberId(comment.getMember().getId());
        response.setVoteBoardId(comment.getVoteBoard().getId());
        response.setContents(comment.getContents());
        response.setRecommendNum(comment.getRecommendNum());
        response.setDateCommentRegister(comment.getDateCommentRegister());

        response.setTopicA(board.getTopicA());
        response.setTopicB(board.getTopicB());

        response.setIsA(voteBoard.getIsA() ? "A투표" : "B투표");

        return response;
    }

    /**
     * 규제 위반 댓글 삭제
     *
     * @param id 댓글의 id
     */
    public void delComment (long id) {
        commentRepository.deleteById(id);
    }
}
