package com.gb.versusapi.service;

import com.gb.versusapi.entity.Member;
import com.gb.versusapi.entity.RecommendBoard;
import com.gb.versusapi.model.recommendBoard.RecommendBoardList;
import com.gb.versusapi.repository.RecommendBoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RecommendBoardService {
    private final RecommendBoardRepository recommendBoardRepository;

    /**
     * 좋아요한 게시글 조회하여 리스트로 보여주기
     * 아직 미완성: 아이디를 입력받아 조회하는 건 구현했지만 좋아요를 했다는 조건은 구현하지 못함
     */
    public List<RecommendBoardList> getIsLikeBoard (Member member) {
         List<RecommendBoard> originList = recommendBoardRepository.findAllByMemberOrderByIsLike(member);

         List<RecommendBoardList> result = new LinkedList<>();

         for (RecommendBoard board : originList) {
             RecommendBoardList addList = new RecommendBoardList();


             addList.setMemberId(board.getMember().getId());
             addList.setBoardId(board.getBoard().getId());
             addList.setIsLike(board.getIsLike() ? "좋아요" : "싫어요");

             result.add(addList);
         }

        return result;
    }
}
