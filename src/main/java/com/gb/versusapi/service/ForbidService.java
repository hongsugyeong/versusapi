package com.gb.versusapi.service;

import com.gb.versusapi.entity.Forbid;
import com.gb.versusapi.model.forbid.ForbidCreateRequest;
import com.gb.versusapi.model.forbid.ForbidList;
import com.gb.versusapi.repository.ForbidRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ForbidService {
    private final ForbidRepository forbidRepository;

    /**
     * 금칙어 생성
     *
     * @param request ForbidCreateRequest
     */
    public void setForbid (ForbidCreateRequest request) {
        Forbid addData = new Forbid();

        addData.setForbidName(request.getForbidName());
        addData.setDateForbid(request.getDateForbid());

        forbidRepository.save(addData);
    }

    /**
     * 금칙어 리스트 조회
     *
     * @return List<ForbidList>
     */
    public List<ForbidList> getForbids () {
        List<Forbid> originForbid = forbidRepository.findAll();

        List<ForbidList> result = new LinkedList<>();

        for (Forbid forbid : originForbid) {
            ForbidList addList = new ForbidList();
            addList.setId(forbid.getId());
            addList.setForbidName(forbid.getForbidName());
            addList.setDateForbid(forbid.getDateForbid());

            result.add(addList);
        }

        return result;
    }

    /**
     * 금칙어 삭제
     *
     * @param id 금칙어 id
     */
    public void delForbid (long id) {
        forbidRepository.deleteById(id);
    }
}
