package com.gb.versusapi.service;

import com.gb.versusapi.entity.VoteBoard;
import com.gb.versusapi.repository.VoteBoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class VoteBoardService {
    private final VoteBoardRepository voteBoardRepository;

    /**
     * 작성한 댓글 조회하여 댓글과 게시글 리스트, 투표한 거 보여주기을 위한 기능
     *
     * @param id 투표한 게시글의 아이디
     * @return VoteBoard
     */
    public VoteBoard getVoteBoardId (long id) {
        return voteBoardRepository.findById(id).orElseThrow();
    }
}
