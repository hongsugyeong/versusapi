package com.gb.versusapi.service;

import com.gb.versusapi.entity.Member;
import com.gb.versusapi.enums.Grade;
import com.gb.versusapi.enums.MemberRating;
import com.gb.versusapi.model.member.MemberDupCheckResponse;
import com.gb.versusapi.model.member.MemberJoinRequest;
import com.gb.versusapi.model.member.MemberNickNameChangeRequest;
import com.gb.versusapi.model.member.MemberPasswordChangeRequest;
import com.gb.versusapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    /**
     * 아이디 중복 확인을 회원가입에 적용하기
     *
     * @param username 아이디
     * @return 새로운지 Boolean
     */
    public MemberDupCheckResponse getMemberIdDupCheck (String username) {
        MemberDupCheckResponse response = new MemberDupCheckResponse();
        response.setIsNew(isNewUsername(username));

        return response;
    }

    /**
     * 회원을 가입시킨다.
     *
     * @param request 회원가입 창에서 고객이 작성한 정보
     * @throws Exception 아이디 중복일 경우, 비밀번호와 비밀번호 확인이 일치하지 않을 경우
     */
    public void setMemberJoin (MemberJoinRequest request) throws Exception {
        if (!isNewUsername(request.getUsername())) throw new Exception(); // b-i
        if (!request.getPassword().equals(request.getPasswordRe())) throw new Exception(); // c-i

        Member addData = new Member();

        addData.setUsername(request.getUsername());
        addData.setPassword(request.getPassword());

        addData.setNickName(request.getNickName());
        addData.setName(request.getName());
        addData.setDateBirth(request.getDateBirth());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setGender(request.getGender());
        addData.setEmail(request.getEmail());

        addData.setMemberRating(MemberRating.MEMBER);
        addData.setGrade(Grade.BRONZE);
        addData.setDateUser(LocalDateTime.now());

        memberRepository.save(addData);
    }

    /**
     * 닉네임 변경하기
     *
     * @param id 변경하고 싶은 id
     * @param request 닉네임 변경 틀
     */
    public void putNickName (long id, MemberNickNameChangeRequest request) {
        Member useNickName = memberRepository.findById(id).orElseThrow();

        useNickName.setNickName(request.getNickName());

        memberRepository.save(useNickName);
    }

    /**
     * 비밀번호 변경하기
     *
     * @throws Exception 비밀번호와 비밀번호 확인이 일치하지 않을 경우
     */
    public void putPassword (long id, MemberPasswordChangeRequest request) throws Exception {
        if (!request.getPassword().equals(request.getPasswordRe())) throw new Exception();

        Member usePassword = memberRepository.findById(id).orElseThrow();

        usePassword.setPassword(request.getPassword());

        memberRepository.save(usePassword);
    }

    /**
     * 신규 아이디인지 확인
     *  서포터 역할
     *
     * @param username 아이디
     * @return true 신규아이디/ false 중복아이디
     */
    private boolean isNewUsername (String username) {
        long dupCount = memberRepository.countByUsername(username);

        return dupCount <= 0;
    }

    public Member getMemberId (long id) {
        return memberRepository.findById(id).orElseThrow();
    }
}
