package com.gb.versusapi.service;

import com.gb.versusapi.lib.CommonFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

@Service
public class TempService {
    public void setRentCarByFile (MultipartFile multipartFile) throws IOException {
        File file = CommonFile.multipartToFile(multipartFile);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        // 파싱 부분

        bufferedReader.close();
    }
}
