package com.gb.versusapi.service;

import com.gb.versusapi.entity.Board;
import com.gb.versusapi.model.board.BoardList;
import com.gb.versusapi.model.forbid.CategoryChangeRequest;
import com.gb.versusapi.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;

    /**
     * 작성한 게시글 리스트로 보여주기
     *
     * @return 보여주고 싶은 게시글 속 리스트
     */
    public List<BoardList> getBoards () {
        List<Board> originBoard = boardRepository.findAll();

        List<BoardList> result = new LinkedList<>();

        for (Board board : originBoard) {
            BoardList addList = new BoardList();
            addList.setMemberId(board.getMember().getId());
            addList.setCategory(board.getCategory());
            addList.setPostType(board.getPostType());
            addList.setTopicA(board.getTopicA());
            addList.setTopicB(board.getTopicB());
            addList.setDateBoard(board.getDateBoard());

            result.add(addList);
        }

        return result;
    }

    /**
     * 작성한 댓글 조회하여 댓글과 게시글 리스트, 투표한 거 보여주기을 위한 기능
     * BoardService => CommentService
     *
     * @param id
     * @return 보드
     */
    public Board getBoardId (long id) {
        return boardRepository.findById(id).orElseThrow();
    }

    /**
     * 잘못된 카테고리 변경
     *
     * @param id 잘못된 카테고리 게시글의 아이디
     * @param request 카테고리 변경 모델
     */
    public void putCategory (long id, CategoryChangeRequest request) {
        Board board = boardRepository.findById(id).orElseThrow();

        board.setCategory(request.getCategory());
    }
}
