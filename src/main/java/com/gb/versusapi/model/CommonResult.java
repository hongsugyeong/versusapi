package com.gb.versusapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonResult {

    private String msg;
    private Integer code;
}
