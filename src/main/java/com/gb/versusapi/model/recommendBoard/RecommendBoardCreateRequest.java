package com.gb.versusapi.model.recommendBoard;

import com.gb.versusapi.entity.Board;
import com.gb.versusapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecommendBoardCreateRequest {
    private Member memberId;
    private Board boardId;
    private Boolean isLike;
}
