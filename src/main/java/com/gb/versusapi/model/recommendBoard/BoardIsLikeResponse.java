package com.gb.versusapi.model.recommendBoard;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardIsLikeResponse {

    private Boolean isRealLike;
}
