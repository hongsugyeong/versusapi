package com.gb.versusapi.model.recommendBoard;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecommendBoardList {
    private Long memberId;
    private Long boardId;
    private String isLike;
}
