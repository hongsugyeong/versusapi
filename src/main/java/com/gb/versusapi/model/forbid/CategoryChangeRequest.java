package com.gb.versusapi.model.forbid;

import com.gb.versusapi.enums.Category;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryChangeRequest {
    private Category category;
}
