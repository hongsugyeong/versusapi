package com.gb.versusapi.model.forbid;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class ForbidList {
    private Long id;
    private String forbidName;
    private LocalDateTime dateForbid;
}
