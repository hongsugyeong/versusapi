package com.gb.versusapi.model.board;

import com.gb.versusapi.enums.Category;
import com.gb.versusapi.enums.PostType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class BoardList {
    private Long memberId;
    private Category category;
    private PostType postType;
    private String topicA;
    private String topicB;
    private LocalDateTime dateBoard;
}
