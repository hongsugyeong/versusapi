package com.gb.versusapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberNickNameChangeRequest {
    private String nickName;
}
