package com.gb.versusapi.model.comment;

import com.gb.versusapi.entity.Board;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CommentResponse {
    private Long memberId;
    private Long voteBoardId;
    private String contents;
    private Long recommendNum;
    private LocalDateTime dateCommentRegister;

    private String topicA;
    private String topicB;

    private String isA;
}
