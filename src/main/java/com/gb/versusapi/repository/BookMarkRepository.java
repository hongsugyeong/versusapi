package com.gb.versusapi.repository;

import com.gb.versusapi.entity.BookMarkTarget;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookMarkRepository extends JpaRepository<BookMarkTarget, Long> {
}
