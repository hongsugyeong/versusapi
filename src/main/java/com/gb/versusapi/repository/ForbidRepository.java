package com.gb.versusapi.repository;

import com.gb.versusapi.entity.Forbid;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ForbidRepository extends JpaRepository<Forbid, Long> {
}
