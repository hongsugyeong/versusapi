package com.gb.versusapi.repository;

import com.gb.versusapi.entity.Member;
import com.gb.versusapi.entity.RecommendBoard;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RecommendBoardRepository extends JpaRepository<RecommendBoard, Long> {
    // 실행됐던 코드: findAllByMemberOrderByIsLike
    List<RecommendBoard> findAllByMemberOrderByIsLike (Member member);
}
