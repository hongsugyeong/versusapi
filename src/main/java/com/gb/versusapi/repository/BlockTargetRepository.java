package com.gb.versusapi.repository;

import com.gb.versusapi.entity.BlockTarget;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BlockTargetRepository extends JpaRepository<BlockTarget, Long> {
}
