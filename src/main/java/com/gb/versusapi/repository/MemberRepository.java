package com.gb.versusapi.repository;

import com.gb.versusapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.swing.text.html.Option;
import java.security.cert.PKIXRevocationChecker;
import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {

    long countByUsername (String username);

    Optional <Member> findByUsername (String username);

}