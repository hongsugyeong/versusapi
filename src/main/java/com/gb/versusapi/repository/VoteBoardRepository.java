package com.gb.versusapi.repository;

import com.gb.versusapi.entity.VoteBoard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VoteBoardRepository extends JpaRepository<VoteBoard, Long> {
}
