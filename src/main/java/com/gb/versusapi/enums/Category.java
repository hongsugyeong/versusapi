package com.gb.versusapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Category {
    DAILY("일상"),
    HUMOR("유머"),
    INFO("정보"),
    ENTERTAINMENT("연예"),
    GAME("게임"),
    ANIMATION("만화"),
    SPORTS("스포츠");

    private final String name;
}
