package com.gb.versusapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PostType {
    LETTER("글"),
    IMAGE("이미지"),
    VIDEO("동영상");

    private final String name;
}
